# Wikimedia toolforge deployment

https://backstage.toolforge.org/

https://toolsadmin.wikimedia.org/tools/id/backstage

## Update Deployment

On the bastion:

```sh
~/src/deploy/pull
~/src/deploy/webservice-shell
```

In the container:

```sh
~/src/deploy/build
exit
```

And back on the bastion:

```sh
~/src/deploy/sync-www
~/src/deploy/webservice-restart
```

## Initial Install

**Clone the code into `src`:**

```sh
git clone https://gitlab.wikimedia.org/addshore/backstage.git src
```

**Start an interactive node session:**

```sh
webservice --backend=kubernetes --cpu 1 --mem 2Gi node12 shell
```

**Install yarn into `yarn`:**

```sh
mkdir yarn
cd ~/yarn
npm install yarn
```

A `yarn` binary is now accessible at `~/yarn/node_modules/.bin/yarn`

**Install and initially build backstage:**

Taking inspiration from https://backstage.io/docs/deployment/docker

```sh
cd ~/src
~/yarn/node_modules/.bin/yarn install --frozen-lockfile --ignore-engines
~/yarn/node_modules/.bin/yarn tsc
~/yarn/node_modules/.bin/yarn build
```

The `--ignore-engines` is currently needed, else we get a `error rollup-plugin-dts@3.0.2: The engine "node" is incompatible with this module. Expected version ">=v12.22.1". Got "12.21.0"`

**Build a deployment:**

```sh
mkdir -p ~/build
cd ~/build
cp ~/src/yarn.lock ~/src/package.json ~/src/packages/backend/dist/skeleton.tar.gz ~/build
tar xzf skeleton.tar.gz && rm skeleton.tar.gz
~/yarn/node_modules/.bin/yarn install --frozen-lockfile --production --network-timeout 300000 --ignore-engines
cp ~/src/packages/backend/dist/bundle.tar.gz ~/src/app-config.yaml ~/src/app-config.production.yaml ~/build
tar xzf bundle.tar.gz && rm bundle.tar.gz
```

Can we make this yarn install any faster by copying something from somewhere?

**Move it to deployment:**

Not in a node / webservice container!

```sh
mkdir -p ~/www/js
rsync -avu --delete /data/project/backstage/build/ /data/project/backstage/www/js/
```

**Run it:**

```sh
webservice --backend=kubernetes node12 start
```
