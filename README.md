# [Backstage](https://backstage.io)

This is your newly scaffolded Backstage App, Good Luck!

To start the locally, run:

```sh
yarn install
yarn dev
```

You can find all of the data that holds this app together in the `data` directory.

This application is deployed to Wikimedia Toolforge, you can read more about that in [the deploy directory](./deploy)
